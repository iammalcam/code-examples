import { HttpService } from '../src/services/http.service';
import { SignedHttpService } from '../src/services/signed-http-service';
import { Credential } from '../src/model/credential';

describe('My list test case', () => {
    it('Should include know value', () => {
        const signedService = new SignedHttpService(
            new HttpService(),
            new Credential('acceskey', 'secrekey'),
        );
        const url = new URL(`https://www.google.com.mx/?hl=es-419`);

        signedService.post(url.toString(), [{ id: 1}])
    });
});
