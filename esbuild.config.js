const esbuild = require("esbuild");

const { nodeExternalsPlugin } = require("esbuild-node-externals");


esbuild
  .build({
    entryPoints: ["./src/index.ts"],
    outfile: "dist/index.js",

    // Don't bundle or minify files, so we can read them
    bundle: true,
    minify: false,

    platform: "node",
    sourcemap: true,
    sourcesContent: true,
    plugins: [nodeExternalsPlugin()],
  })
  .catch(() => {
    process.exit(1);
  });
