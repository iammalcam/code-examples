import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { Exception } from '../model/exception';
import { Http } from '../contracts/http';

export type Result<T> = { result: T; err: Exception | null };
export type HttpServiceResponse = {
  request: any;
  response: AxiosResponse | null;
};

export class HttpService implements Http {
  private readonly instance;

  get axiosRef(): AxiosInstance {
    return this.instance;
  }

  constructor(instance?: AxiosInstance) {
    if (instance) {
      this.instance = instance;
    } else {
      this.instance = axios.create({
        timeout: 1000,
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
      });
    }
  }

  async get<T = any>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<Result<HttpServiceResponse>> {
    try {
      return { result: await this.instance.get(url, config), err: null };
    } catch (error) {
      return Promise.resolve(this.handleErrors(error));
    }
  }

  async post<T = any>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<Result<HttpServiceResponse>> {
    try {
      const response = await this.instance.post(url, data, config);
      return {
        result: { request: response.config, response },
        err: null,
      };
    } catch (error) {
      return Promise.resolve(this.handleErrors(error));
    }
  }

  // eslint-disable-next-line class-methods-use-this
  protected handleErrors(error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // console.log(error.response.data);
      // console.log(error.response.status);
      // console.log(error.response.headers);
      return {
        result: { request: error.config, response: error.response },
        err: new Exception(error),
      };
    }
    if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      return {
        result: { request: error.config, response: null },
        err: new Exception(error),
      };
    }
    // Something happened in setting up the request that triggered an Error
    return { result: { request: error.config, response: null }, err: new Exception(error) };
  }
}
