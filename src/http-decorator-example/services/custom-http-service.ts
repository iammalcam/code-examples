import { Http } from '../contracts/http';
import { HttpServiceResponse, Result } from './http.service';

export class CustomHttpService implements Http {
  private readonly decorated;

  constructor(decorated: Http) {
    this.decorated = decorated;
  }

  async get(url: string, config?: any): Promise<Result<HttpServiceResponse>> {
    return this.decorated.get(url, config);
  }

  async post(url: string, data?: any, config?: any): Promise<Result<HttpServiceResponse>> {
    return this.decorated.post(url, data, config);
  }
}
