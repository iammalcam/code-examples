import { Encrypter } from './encrypter';

export class Signer {
  // eslint-disable-next-line class-methods-use-this
  sign(secret: string, message: string[]) {
    return Encrypter.computeHash(secret, message.join(''));
  }
}
