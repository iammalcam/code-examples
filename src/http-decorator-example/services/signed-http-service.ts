import * as _ from 'lodash';
import { CustomHttpService } from './custom-http-service';
import { Http } from '../contracts/http';
import { Credential } from '../model/credential';
import { Signer } from './signer';
import * as crypto from 'crypto';

export class SignedHttpService extends CustomHttpService {
  constructor(decorated: Http, private readonly credentials: Credential) {
    super(decorated);
  }

  // TODO: especificar tipo de retirno menor o igual que parent; liskov
  async post(url: string, data?: any, config?: any): Promise<any> {
    const signer = new Signer();

    const publicKey = this.credentials.accessKey();
    const requestId = crypto.randomUUID();
    const timestamp = new Date().getTime();
    const payload = _.isEmpty(data) ? '' : JSON.stringify(data);

    const messageSignature = signer.sign(this.credentials.secretKey(), [
      publicKey,
      requestId,
      timestamp.toString(),
      payload,
    ]);

    const mergedConfig = {
      ...config,
      ...{
        headers: {
          'X-API-KEY': publicKey,
          'Client-Request-Id': requestId,
          'Message-Signature': messageSignature,
          Timestamp: timestamp,
        },
      },
    };
    return super.post(url, data, mergedConfig);
  }
}
