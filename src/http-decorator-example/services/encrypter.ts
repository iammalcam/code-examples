import crypto from 'crypto';

export class Encrypter {
  public static computeHash(secret: string, payload: string): string {
    const hmac = crypto.createHmac('sha256', secret);
    hmac.write(payload);
    hmac.end();
    return hmac.read().toString('base64');
  }
}
