export interface Http {
  get(url: string, config?: any): Promise<unknown>;

  post(url: string, data?: any, config?: any): Promise<unknown>;
}
