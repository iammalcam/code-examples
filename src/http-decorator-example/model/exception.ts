export class Exception extends Error {
  protected error: string | Exception;

  constructor(error) {
    super();
    this.error = error;
    this.initMessage();
  }

  getError() {
    return this.error;
  }

  protected initMessage() {
    function isObject(obj): obj is Record<string, any> {
      return typeof obj === 'object' && !Array.isArray(obj) && obj !== null;
    }

    function isString(str: any): str is string {
      return typeof str === 'string' || str instanceof String;
    }

    if (isString(this.error)) {
      this.message = this.error;
    } else if (isObject(this.error) && isString(this.error.message)) {
      this.message = this.error.message;
    } else if (this.constructor) {
      this.message = this.constructor.name.match(/[A-Z][a-z]+|[0-9]+/g).join(' ');
    }
  }
}
