export class Credential {
  private _id: number;

  private readonly _accessKey: string;

  private readonly _secretKey: string;

  private _createdAt: Date;

  constructor(accessKey?: string, secretKey?: string) {
    if (accessKey && secretKey) {
      this._accessKey = accessKey;
      this._secretKey = secretKey;
      this._createdAt = new Date(); // new Date().toISOString(),
    }
  }

  public secretKey() {
    return this._secretKey;
  }

  public accessKey() {
    return this._accessKey;
  }
}
