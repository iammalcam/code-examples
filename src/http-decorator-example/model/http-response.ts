export class HttpResponse {
  statusCode?: number | undefined;

  headers?:
    | {
        [header: string]: boolean | number | string;
      }
    | undefined;

  body?: string | undefined;
}
