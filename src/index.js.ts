import { HttpService } from './services/http.service';
import { SignedHttpService } from './services/signed-http-service';
import { Credential } from './model/credential';

const signedService = new SignedHttpService(
    new HttpService(),
    new Credential('acceskey', 'secrekey'),
);
const url = new URL(`/admin/dashboard/sync`);

signedService.post(url.toString(), [{ id: 1}])
